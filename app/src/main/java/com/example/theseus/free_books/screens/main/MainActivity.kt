package com.example.theseus.free_books.screens.main

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.theseus.free_books.Constants
import com.example.theseus.free_books.R
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem
import com.example.theseus.free_books.screens.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity(),IMainView {
    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun openDetailActivity(movieId: String) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(Constants.MOVIE_ID, movieId)
        startActivity(intent)
    }

    override fun populateList(results: List<ResultsItem?>) {
        mAdapter.updateList(results)
    }

    override fun showToast(localizedMessage: String?) {
        localizedMessage?.let {
            toast(it)
        }?:run {
            toast( getString(R.string.somethin_went_wrong))
        }
    }
    lateinit var mPresenter:IMainPresenter
    val mAdapter = MoviesAdapter(this,object : MoviesAdapter.MovieItemClicked{
        override fun onClick(item: ResultsItem) {
            mPresenter.movieItemClicked(item)
        }
    })
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        book_list.layoutManager = LinearLayoutManager(this)
        book_list.adapter = mAdapter
        mPresenter = MainPresenter()
        mPresenter.setView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }
}
