package com.example.theseus.free_books.screens.main

import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem

interface IMainView {
    fun showToast(localizedMessage: String?)
    fun populateList(results: List<ResultsItem?>): Unit?
    fun openDetailActivity(movieId: String)
    fun hideProgressBar()
    fun showProgressBar()
}