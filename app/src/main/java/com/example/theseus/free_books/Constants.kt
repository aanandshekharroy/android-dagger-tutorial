package com.example.theseus.free_books

class Constants{
    companion object {
        val EXTRA_BOOK_ITEM = "extra_book_item"
        val MOVIE_ID = "movie_id"
    }
}