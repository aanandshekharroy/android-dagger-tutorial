package com.example.theseus.free_books.screens.main

import com.example.theseus.free_books.data.DataManager
import com.example.theseus.free_books.data.IDataManager
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.MovieResponse
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter :IMainPresenter {
    override fun movieItemClicked(item: ResultsItem) {
        mView?.openDetailActivity(item.id.toString())
    }

    var mView:IMainView ?=null
    lateinit var mDataManager:IDataManager
    override fun setView(mainView: IMainView) {
        mDataManager = DataManager()
        mView = mainView

        getTopRatedMovies()
    }
    fun getTopRatedMovies(){
        mView?.showProgressBar()
        mDataManager.fetchTopRatedMovies().enqueue(
                object : Callback<MovieResponse>{
                    override fun onFailure(call: Call<MovieResponse>?, t: Throwable?) {
                        mView?.hideProgressBar()
                        mView?.showToast(t?.localizedMessage)
                    }

                    override fun onResponse(call: Call<MovieResponse>?, response: Response<MovieResponse>?) {

                        response?.body()?.results?.let {
                            mView?.populateList(it)
                        }?:run{
                            mView?.showToast(response?.message())
                        }
                        mView?.hideProgressBar()
                    }

                }
        )

    }
    override fun detachView() {
        mView = null
    }

}