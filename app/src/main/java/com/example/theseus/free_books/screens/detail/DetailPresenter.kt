package com.example.theseus.free_books.screens.detail

import android.util.Log
import com.example.theseus.free_books.data.DataManager
import com.example.theseus.free_books.data.IDataManager
import com.example.theseus.free_books.data.model.DetailMovieResponse.DetailedMovieResponse
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPresenter:IDetailPresenter {
    lateinit var mDataManager: IDataManager
    override fun setMovieDetail(movieId: String) {
        mView?.showProgressBar()
        mDataManager.fetchMovieDetail(movieId).enqueue(object : Callback<DetailedMovieResponse>{
            override fun onFailure(call: Call<DetailedMovieResponse>?, t: Throwable?) {
                mView?.showToast(t?.localizedMessage)
                mView?.hideProgressBar()
            }

            override fun onResponse(call: Call<DetailedMovieResponse>?, response: Response<DetailedMovieResponse>?) {
                response?.body()?.let {
                    mView?.setBackDropImage("http://image.tmdb.org/t/p/w342/${it.backdropPath}")
                    mView?.setMovieTitle(it.title!!)
                    mView?.setOverview(it.overview)
                    mView?.setVoteAverage(it.voteAverage.toString())
                    mView?.hideProgressBar()
                }?:kotlin.run {
                    mView?.hideProgressBar()
                }


            }

        })
    }



    var mView:IDetailView?=null
    override fun setView(view: IDetailView) {
        mView = view
        mDataManager = DataManager()
    }

    override fun detachView() {
        mView = null
    }

}