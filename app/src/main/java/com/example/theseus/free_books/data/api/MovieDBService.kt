package com.example.theseus.free_books.data.api

import com.example.theseus.free_books.BuildConfig
import com.example.theseus.free_books.data.model.DetailMovieResponse.DetailedMovieResponse
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.MovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieDBService {
    @GET("top_rated?api_key=${BuildConfig.API_KEY}&language=en-US")
    fun getTopRatedMovies():Call<MovieResponse>

    @GET("{movie_id}?api_key=${BuildConfig.API_KEY}")
    fun getMovieDetail(@Path("movie_id") movie_id:String):Call<DetailedMovieResponse>
}