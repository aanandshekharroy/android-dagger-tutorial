package com.example.theseus.free_books.screens.main

import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem

interface IMainPresenter {
    fun setView(mainView:IMainView)
    fun detachView()
    fun movieItemClicked(item: ResultsItem)
}