package com.example.theseus.free_books.screens.detail

import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem

interface IDetailPresenter {
    fun setView(view:IDetailView)
    fun detachView()
    fun setMovieDetail(movieId: String)
}