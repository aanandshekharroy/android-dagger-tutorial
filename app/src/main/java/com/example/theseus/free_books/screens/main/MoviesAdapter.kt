package com.example.theseus.free_books.screens.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.theseus.free_books.R
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem
import com.squareup.picasso.Picasso

class MoviesAdapter(val mContext:Context,var mListener:MovieItemClicked):RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>() {

    interface MovieItemClicked {
        fun onClick(item: ResultsItem)
    }
    var mBookList = emptyList<ResultsItem?>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.movie_item,parent,false))

    override fun getItemCount()= mBookList.size


    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        mBookList.get(position)?.let {
            Picasso.with(mContext).load("http://image.tmdb.org/t/p/w342/${it.posterPath}").into(holder.moviePoster)
            holder.bind(it,mListener)
        }

    }

    class MovieViewHolder(var view: View):RecyclerView.ViewHolder(view){
        val moviePoster:ImageView

        init {
            moviePoster = view.findViewById(R.id.poster)
        }

        fun bind(movieItem: ResultsItem, mListener: MovieItemClicked) {
            view.setOnClickListener {
                mListener.onClick(movieItem)
            }

        }
    }

    fun updateList(results: List<ResultsItem?>) {
        mBookList = results
        notifyDataSetChanged()
    }
}