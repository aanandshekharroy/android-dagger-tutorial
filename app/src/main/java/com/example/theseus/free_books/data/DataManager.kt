package com.example.theseus.free_books.data

import com.example.theseus.free_books.data.api.MovieDBService
import com.example.theseus.free_books.data.model.DetailMovieResponse.DetailedMovieResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class DataManager:IDataManager {
    val BASE_URL = "https://api.themoviedb.org/3/movie/"
    var retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    val movieService = retrofit.create(MovieDBService::class.java)
    override fun fetchTopRatedMovies() = movieService.getTopRatedMovies()
    override fun fetchMovieDetail(movieId: String) = movieService.getMovieDetail(movieId)

}