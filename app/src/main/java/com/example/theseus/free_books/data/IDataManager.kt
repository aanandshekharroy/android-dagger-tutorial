package com.example.theseus.free_books.data

import com.example.theseus.free_books.data.model.DetailMovieResponse.DetailedMovieResponse
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.MovieResponse
import retrofit2.Call

interface IDataManager {
    fun fetchTopRatedMovies():Call<MovieResponse>
    fun fetchMovieDetail(movieId:String):Call<DetailedMovieResponse>
}