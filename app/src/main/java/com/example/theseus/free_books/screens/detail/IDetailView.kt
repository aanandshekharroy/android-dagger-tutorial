package com.example.theseus.free_books.screens.detail

interface IDetailView {
    fun setBackDropImage(path:String)
    fun setMovieTitle(title:String)
    fun showProgressBar()
    fun showToast(localizedMessage: String?)
    fun hideProgressBar()
    fun setOverview(overview: String?)
    fun setVoteAverage(voteAverage: String?)
}