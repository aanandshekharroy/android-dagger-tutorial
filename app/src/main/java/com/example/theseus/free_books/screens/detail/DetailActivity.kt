package com.example.theseus.free_books.screens.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.theseus.free_books.Constants
import com.example.theseus.free_books.R
import com.example.theseus.free_books.data.model.TopRatedMoviesResponse.ResultsItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.toast

class DetailActivity : AppCompatActivity(),IDetailView {
    override fun setOverview(overview_text: String?) {
        overview.text = overview_text

    }

    override fun setVoteAverage(voteAverage: String?) {
        vote_average.text  = voteAverage
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }
    override fun showToast(localizedMessage: String?) {
        localizedMessage?.let {
            toast(it)
        }?:run {
            toast( getString(R.string.somethin_went_wrong))
        }
    }
    override fun setBackDropImage(path:String) {
        Picasso.with(this).load(path).into(backdrop)
    }

    override fun setMovieTitle(title: String) {
        movieTitle.text = title
    }

    lateinit var  mPresenter:IDetailPresenter
    lateinit var mResult: ResultsItem
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        mPresenter = DetailPresenter()
        mPresenter.setView(this)
        mPresenter.setMovieDetail(intent.getStringExtra(Constants.MOVIE_ID))
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }
}
